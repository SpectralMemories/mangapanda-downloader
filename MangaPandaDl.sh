#!/bin/bash
#
# MangaPanda Downloader written by SpectralMemories
# (aaalex12gagne@gmail.com)
# On October 21 2018
# 
# Allow an automatic download of entire manga series from MangaPanda.com
# See https://gitlab.com/SpectralMemories/mangapanda-downloader for more info
#
# Licensed under the MIT license. See Gitlab page for full license
#
# This script comes with absolutely no warranty. Feel free to report any
# Bugs or suggestions in the gitlab issues

if [ "$1" == "--help" ]
then
    echo "Downloads a series on mangapanda"
    echo "MangaPandaDl.sh [name of manga] [folder to download to] [(optional) Start from chapter...]"
    exit 0
fi

if [ "$1" == "" ]
then
    echo "Error: you must specify a manga from mangapanda to download"
    exit 1
fi

if [ "$2" == "" ]
then
    echo "Error: you must specify a directory do download to"
    exit 3
fi

startChapter=1
if [ "$3" != "" ]
then
    startChapter=$3
fi

echo "Searching for $1 on mangapanda.com..."
confirmation=$(curl -s --head "http://www.mangapanda.com/$1" | grep 'HTTP')
confirmation=$(echo $confirmation | grep -E -o "[0-9]+" | tail -1)

if [ "$confirmation" != "200" ]
then
    echo "Error: manga $1 was not found on mangapanda.com"
    exit 2
fi

echo "$1 found!"



Url="http://www.mangapanda.com/$1"
RootFolder="$2"

#Parameter: image url [string], folder [string], filename [string]
#Depends: Wget
DownloadPage () {
    imgurl=$1
    location="$2"
    filename=$3
    if [ ! -f "$location/$filename" ]
    then
        wget -q -O "$location/$filename" $wantedUrl
        echo "Downloaded file $filename"
    else
        echo "File $filename already exists. Skipping"
    fi
}

#Parameter: chapter [int], series URL [string], root folder [string]
#Depends: DownloadPage, Curl, Grep
DownloadChapter () {
    currentPage=1
    chapter=$1
    seriesUrl=$2
    rootFolder="$3"

    echo "Downloading chapter $chapter ..."

    #Get page count in chapter
    sourceCode=$(curl -Ss $seriesUrl/$chapter | grep '</select> of ')
    maxPage=$(echo $sourceCode | grep -E -o "[0-9]+")

    if [ "$maxPage" == "" ]
    then
        echo "Warning: no entry found for chapter $chapter. Skipping it.."
        return
    fi
    #
    #Make folder
    mkdir "$rootFolder/Chapter_$chapter"
    echo "folder $rootFolder/Chapter_$chapter created"
    #
    while (($currentPage <= $maxPage )) #Loops pages in chapter
    do
        currentUrl="$seriesUrl/$chapter/$currentPage"
        line=$(curl -Ss $currentUrl | grep ' - Page ')
        wantedUrl=$(echo $line | sed -ne 's/.*\(http[^"]*\).*/\1/p')
        DownloadPage $wantedUrl "$rootFolder/Chapter_$chapter" "Page_$currentPage.jpg"
        currentPage=$(($currentPage + 1))
    done
    echo "Downloaded chapter $chapter !"
}

#Parameter: series Url [string], rootFolder [string], series Name [string], chapterCount [int]
#Depends: Touch, Curl, Grep, Date
CreateInfo () {
    #Isolate reading direction

    readingDirection=$(curl -Ss $1 | grep "Right to Left")

    if [ "$readingDirection" == "" ] #cheap method, but effective
    then
        readingDirection="Left to Right"
    else
        readingDirection="Right to Left"
    fi

    #Readme file
    echo "Writing info file at $2/Readme.txt..."

    touch "$2/Readme.txt"
    echo "Downloaded via SpectralMemories' MangaPandaDl.sh script" > "$2/Readme.txt"
    echo "See https://gitlab.com/SpectralMemories/mangapanda-downloader" >> "$2/Readme.txt"
    echo "" >> "$2/Readme.txt"
    echo "" >> "$2/Readme.txt"
    echo "======================" >> "$2/Readme.txt"
    echo "Title: $3" >> "$2/Readme.txt"
    echo "Chapter count: $4" >> "$2/Readme.txt"
    echo "Reading direction: $readingDirection" >> "$2/Readme.txt"
    echo "Downloaded on: $(date)" >> "$2/Readme.txt"
    echo "======================" >> "$2/Readme.txt"

    echo "Readme.txt created!"
    #
}

echo "Looking for chapters for $1"

#Get chapter count
mangaPageSource=$(curl -Ss $Url | grep "/$1/" | tail -1)
maxChapter=$(echo $mangaPageSource | grep -E -o "[0-9]+" | tail -1)

if [ "$maxChapter" == "" ]
then
    echo "Error: $1 has no chapter.."
    exit 5
else
    echo "Found $maxChapter chapters for $1 !"
fi

CreateInfo $Url "$RootFolder" $1 $maxChapter


currentChapter=$startChapter
while (($currentChapter <= $maxChapter))
do
    DownloadChapter $currentChapter $Url "$RootFolder"
    currentChapter=$(($currentChapter + 1))
done

echo "Done. Downloaded all chapters of $Url"
exit 0