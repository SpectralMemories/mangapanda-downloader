# MangaPanda Downloader

This is a bash script that allows automatic downloads of mangas on mangapanda.com

# Usage

Run the script as follow:

./MangaPandaDl.sh [name of the manga *] [folder to download to]

* Note here that this is the name as it appears in the url, after "mangapanda.com/[XYZ]"

# Demo

A video demo can be seen here: https://drive.google.com/file/d/1Ys3Y1H0uGLavOKZIWlyDg5J9tutfsYdz/view?usp=sharing

# Dependencies  

This script requires:
    
    * An internet connection (duh)
    * An up to date bash interpreter
    * Curl
    * Wget
    * Sed
    
# Windows

Full instruction video available [here](https://www.youtube.com/watch?v=3hfqFU6l-vM) (excuse the terrible audio, my mic sucks)

Even though this script was intended to be used on GNU/Linux operating systems,
it is possible to run it via cygwin (https://www.cygwin.com/). It is a bash interpreter
implementation for Windows. Be sure to check all the dependencies listed above, as they
may not be included by default.