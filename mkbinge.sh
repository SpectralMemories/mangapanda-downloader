#!/bin/bash
#
# This script allows

if [ "$1" == "--help" ]
then
    echo "This script allows the creation of a folder of symlinks (or copies) for binge watching"
    echo "$0 /home/Joe/Pictures/Naruto [starting chapter] [--copy] [--continue] [--topdf (requires img2pdf)]"
    exit 0
fi

if [ "$1" == "" ]
then
    echo "Error: please specify a folder to operate on"
    exit 2
fi

skipped=0

folder="$1"

cd "$folder"

if [ ! -d "./binge" ]
then
    mkdir binge
else
    rm -r ./binge/*
fi

is_valid=False
currentChapter=1
if [ "$2" != "" ] && [ "$2" != "--copy" ] && [ "$2" != "--continue" ] && [ "$2" != "--topdf" ]
then
    currentChapter=$2
fi
currentPage=1

while [ True ]
do
    if [ ! -d "Chapter_$currentChapter" ]
    then
        if [[ "$@" == *"--continue"* ]]
        then
            currentChapter=$(($currentChapter + 1))
            skipped=$(($skipped + 1))
            
            if (($skipped > 100))
            then
                break
            fi
            continue
        else
            break
        fi
    fi
    is_valid=True

    currentChapterPage=1
    while [ True ]
    do
        page="Chapter_$currentChapter/Page_$currentChapterPage.jpg"
        if [ ! -f "$page" ]
        then
            break
        fi

        #cp "$page" "$folder/binge/Page_$currentPage.jpg"
        if [[ "$@" == *"--copy"* ]]
        then
            cp -fv "$page" "./binge/Page_$currentPage.jpg"
        else
            ln -sv "../$page" "./binge/Page_$currentPage.jpg"
        fi

        currentChapterPage=$(($currentChapterPage + 1))
        currentPage=$(($currentPage + 1))
    done
    
    currentChapter=$(($currentChapter + 1))
done

if [ $is_valid == False ]
then
    echo "Error: folder is not a mangapandadl folder"
    exit 1
elif [[ "$@" == *"--topdf"* ]]
then
    cd ./binge
    echo "Syncing..."
    sync
    echo "Converting to PDF..."
    img2pdf $(ls -v | paste -sd\ ) -o binge.pdf
    sync
fi

echo "Done"